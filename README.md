# pokemonService

Consumo de api pokemon para challenge

En este proyecto consumimos el api expuesto de pokemon, el cual tiene la siguiente URL:

https://pokeapi.co/api/v2/pokemon/

En base a la respuesta por cada pokemon se creo los siguientes metodos

-abilities
-base_experience
-held_items
-id
-name
-location_area_encounters

Configuracion necesarias

-Tener configurado Maven
-Java 8
-Tener instalado Mongodb y correrlo de manera local en el puerto 27017(default)

