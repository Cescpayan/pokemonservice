package com.example.spring.soap.api.db.mongo;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.mongodb.BasicDBObject;
import com.mongodb.ConnectionString;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class MongoDB {
	public static MongoClient mongoClient;
	public static DB database;
	public static DBCollection tablaHistorial;
	
	public void InsertarRequest(String sRequest,String sMetodo) {
		try {
			InetAddress myIp=InetAddress.getLocalHost();
			//DateTimeFormatter dtf= DateTimeFormatter.ofPattern("yyy/MM/ff HH:mm");
			//onnectionString url= new ConnectionString("mongodb:://127.0.0.1:27017");
			mongoClient= new MongoClient(new MongoClientURI("mongodb://127.0.0.1:27017"));
			database=mongoClient.getDB("mongodbHistorial");
			tablaHistorial=database.getCollection("Historial");
			
			DBObject obj=new BasicDBObject("iporigen",myIp.getHostAddress()).append("fecha", LocalDateTime.now()).append("metodo", sMetodo).append("request", sRequest);
			tablaHistorial.insert(obj);
			//query
			DBObject query= new BasicDBObject("iporigen",myIp);
			DBCursor cursor= tablaHistorial.find(query);
			
			
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}
	
}
