package com.example.spring.soap.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenerationVi{
    @JsonProperty("omegaruby-alphasapphire") 
    public OmegarubyAlphasapphire omegarubyAlphasapphire;
    @JsonProperty("x-y") 
    public XY xY;
}
