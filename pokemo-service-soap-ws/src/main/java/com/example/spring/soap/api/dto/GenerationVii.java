package com.example.spring.soap.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenerationVii{
    public Icons icons;
    @JsonProperty("ultra-sun-ultra-moon") 
    public UltraSunUltraMoon ultraSunUltraMoon;
}
