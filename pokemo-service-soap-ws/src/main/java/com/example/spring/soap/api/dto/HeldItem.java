package com.example.spring.soap.api.dto;

import java.util.List;

public class HeldItem{
    public Item item;
    public List<VersionDetail> version_details;
}
