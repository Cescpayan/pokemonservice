package com.example.spring.soap.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Other{
    public DreamWorld dream_world;
    @JsonProperty("official-artwork") 
    public OfficialArtwork officialArtwork;
}
