package com.example.spring.soap.api.dto;

import java.util.List;



public class PokemonBody{
    public List<Abilities> abilities;
    public int base_experience;
    public List<Form> forms;
    public List<GameIndice> game_indices;
    public int height;
    public List<HeldItem> held_items;
    public int id;
    public boolean is_default;
    public String location_area_encounters;
    public List<Move> moves;
    public String name;
    public int order;
    public List<Object> past_types;
    public Species species;
    public Sprites sprites;
    public List<Stat> stats;
    public List<Type> types;
    public int weight;
	public List<Abilities> getAbilities() {
		return abilities;
	}
	public void setAbilities(List<Abilities> abilities) {
		this.abilities = abilities;
	}
	public int getBase_experience() {
		return base_experience;
	}
	public void setBase_experience(int base_experience) {
		this.base_experience = base_experience;
	}
	public List<Form> getForms() {
		return forms;
	}
	public void setForms(List<Form> forms) {
		this.forms = forms;
	}
	public List<GameIndice> getGame_indices() {
		return game_indices;
	}
	public void setGame_indices(List<GameIndice> game_indices) {
		this.game_indices = game_indices;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public List<HeldItem> getHeld_items() {
		return held_items;
	}
	public void setHeld_items(List<HeldItem> held_items) {
		this.held_items = held_items;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isIs_default() {
		return is_default;
	}
	public void setIs_default(boolean is_default) {
		this.is_default = is_default;
	}
	public String getLocation_area_encounters() {
		return location_area_encounters;
	}
	public void setLocation_area_encounters(String location_area_encounters) {
		this.location_area_encounters = location_area_encounters;
	}
	public List<Move> getMoves() {
		return moves;
	}
	public void setMoves(List<Move> moves) {
		this.moves = moves;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public List<Object> getPast_types() {
		return past_types;
	}
	public void setPast_types(List<Object> past_types) {
		this.past_types = past_types;
	}
	public Species getSpecies() {
		return species;
	}
	public void setSpecies(Species species) {
		this.species = species;
	}
	public Sprites getSprites() {
		return sprites;
	}
	public void setSprites(Sprites sprites) {
		this.sprites = sprites;
	}
	public List<Stat> getStats() {
		return stats;
	}
	public void setStats(List<Stat> stats) {
		this.stats = stats;
	}
	public List<Type> getTypes() {
		return types;
	}
	public void setTypes(List<Type> types) {
		this.types = types;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
}
