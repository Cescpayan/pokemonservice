package com.example.spring.soap.api.dto;
public class VersionGroupDetail{
    public int level_learned_at;
    public MoveLearnMethod move_learn_method;
    public VersionGroup version_group;
}
