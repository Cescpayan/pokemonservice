package com.example.spring.soap.api.endpoint;


import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBElement;

import org.example.estructura.AbilitiRequest;
import org.example.estructura.AbilitiResponse;
import org.example.estructura.Abilities;
import org.example.estructura.BaseExperienceRequest;
import org.example.estructura.BaseExperienceResponse;
import org.example.estructura.BaseResponse;
import org.example.estructura.HeldItemsRequest;
import org.example.estructura.HeldItemsResponse;
import org.example.estructura.IdRequest;
import org.example.estructura.IdResponse;
import org.example.estructura.LocationAreaEncountersRequest;
import org.example.estructura.LocationAreaEncountersResponse;
import org.example.estructura.NameRequest;
import org.example.estructura.NameResponse;
import org.example.estructura.PokemonAbilitiRequest;
import org.example.estructura.PokemonAbilitiResponse;
import org.example.estructura.PokemonBaseResponse;
import org.example.estructura.PokemonRequest;
import org.example.estructura.PokemonRequestAbility;
import org.example.estructura.PokemonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.example.spring.soap.api.db.mongo.MongoDB;
import com.example.spring.soap.api.dto.PokemonBody;
import com.example.spring.soap.api.services.PokemonService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.format.DateTimeFormatter;
import java.net.DatagramSocket;

@Endpoint
public class PokemonEndpoint {
	private static final String NAMESPACE = "http://www.example.org/estructura";
	@Autowired
	private PokemonService service;
	
	

	@PayloadRoot(namespace = NAMESPACE, localPart = "PokemonRequest")
	@ResponsePayload
	public PokemonResponse getBasePokemon(@RequestPayload PokemonRequest request) throws Exception {
		PokemonBody pokeBody=new PokemonBody();
		MongoDB MongoDao= new MongoDB();
		MongoDao.InsertarRequest(request.toString(),"PokemonRequest");
		PokemonResponse pokemonResponse= new PokemonResponse();
		
		pokeBody=service.getBodyPokemon(request.getPokemonName());
		
		//pokemonResponse.setAbilities(pokeBody.getAbilities());
		pokemonResponse.setBaseExperience(Integer.toString( pokeBody.getBase_experience()));
		pokemonResponse.setHeldItems(pokeBody.getHeld_items().toString());
		pokemonResponse.setApprovedAmount(0);
		//pokemonResponse.setId(pokeBody.getId());
		pokemonResponse.setName(pokeBody.getName());
		pokemonResponse.setIsEligible(false);
		pokemonResponse.setLocationAreaEncounters(pokeBody.getLocation_area_encounters());
		
		
		return pokemonResponse;
	}
	
	@PayloadRoot(namespace = NAMESPACE, localPart = "AbilitiRequest")
	@ResponsePayload
	public AbilitiResponse abilities(@RequestPayload AbilitiRequest request) throws Exception
	{
		MongoDB MongoDao= new MongoDB();
		MongoDao.InsertarRequest(request.toString(),"abilities");
		AbilitiResponse pokeomn= new AbilitiResponse();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
		
		PokemonBody pokeBody=new PokemonBody();
		JAXBElement<AbilitiResponse> pokemonFinal=null;
		AbilitiResponse pokemonResponse = new AbilitiResponse();
		
		pokeBody=service.getBodyPokemon(request.getPokemonName());

		
		
		pokemonResponse.setAbilities(service.validarAbiliti(request.getPokemonName(),pokeBody));
		
		
		
		
		return pokemonResponse;
	}
	@PayloadRoot(namespace = NAMESPACE, localPart = "Base_experienceRequest")
	@ResponsePayload
	public BaseExperienceResponse base_experience(@RequestPayload BaseExperienceRequest request) throws Exception {
		BaseExperienceResponse pokeomn= new BaseExperienceResponse();
		MongoDB MongoDao= new MongoDB();
		MongoDao.InsertarRequest(request.toString(),"base_experience");
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
		
		PokemonBody pokeBody=new PokemonBody();
		JAXBElement<AbilitiResponse> pokemonFinal=null;
		BaseExperienceResponse pokemonResponse = new BaseExperienceResponse();
		
		pokeBody=service.getBodyPokemon(request.getPokemonName());

		
		
		pokemonResponse.setBaseExperience(Integer.toString( pokeBody.getBase_experience()));
		
		
		return pokemonResponse;
	}
	@PayloadRoot(namespace = NAMESPACE, localPart = "Held_itemsRequest")
	@ResponsePayload
	public HeldItemsResponse held_items(@RequestPayload HeldItemsRequest request) {
		
		MongoDB MongoDao= new MongoDB();
		MongoDao.InsertarRequest(request.toString(),"held_items");
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
		
		PokemonBody pokeBody=new PokemonBody();
		JAXBElement<AbilitiResponse> pokemonFinal=null;
		HeldItemsResponse pokemonResponse = new HeldItemsResponse();
		
		try {
			pokeBody=service.getBodyPokemon(request.getPokemonName());
			pokemonResponse.setHeldItem(service.validarHeldItems(request.getPokemonName(),pokeBody));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		

		
		
		return pokemonResponse;
	}
	@PayloadRoot(namespace = NAMESPACE, localPart = "IdRequest")
	@ResponsePayload
	public IdResponse id(@RequestPayload IdRequest request) {
		IdResponse pokeomn= new IdResponse();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
		MongoDB MongoDao= new MongoDB();
		MongoDao.InsertarRequest(request.toString(),"id");
		PokemonBody pokeBody=new PokemonBody();
		JAXBElement<AbilitiResponse> pokemonFinal=null;
		IdResponse pokemonResponse = new IdResponse();
		
		try {
			pokeBody=service.getBodyPokemon(request.getPokemonName());
			pokemonResponse.setId(Integer.toString( pokeBody.getId()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		return pokemonResponse;
	}
	@PayloadRoot(namespace = NAMESPACE, localPart = "NameRequest")
	@ResponsePayload
	public NameResponse name(@RequestPayload NameRequest request) {
		NameResponse pokeomn= new NameResponse();
		MongoDB MongoDao= new MongoDB();
		MongoDao.InsertarRequest(request.toString(),"name");
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
		
		PokemonBody pokeBody=new PokemonBody();
		JAXBElement<AbilitiResponse> pokemonFinal=null;
		NameResponse pokemonResponse = new NameResponse();
		
		try {
			pokeBody=service.getBodyPokemon(request.getPokemonName());
			pokemonResponse.setName( pokeBody.getName().toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		

		
		return pokemonResponse;
	}
	@PayloadRoot(namespace = NAMESPACE, localPart = "location_area_encountersRequest")
	@ResponsePayload
	public LocationAreaEncountersResponse location_area_encounters(@RequestPayload LocationAreaEncountersRequest request) {
		LocationAreaEncountersResponse pokeomn= new LocationAreaEncountersResponse();
		MongoDB MongoDao= new MongoDB();
		MongoDao.InsertarRequest(request.toString(),"location_area_encounters");
		try {
			InetAddress myIp=InetAddress.getLocalHost();
			DateTimeFormatter dtf= DateTimeFormatter.ofPattern("yyy/MM/ff HH:mm");
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
		
		PokemonBody pokeBody=new PokemonBody();
		JAXBElement<AbilitiResponse> pokemonFinal=null;
		LocationAreaEncountersResponse pokemonResponse = new LocationAreaEncountersResponse();
		
		try {
			pokeBody=service.getBodyPokemon(request.getPokemonName());
			pokemonResponse.setLocationAreaEncounters( pokeBody.getLocation_area_encounters());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pokemonResponse;
	}
	

public String getRemortIP(HttpServletRequest request) {
  if (request.getHeader("x-forwarded-for") == null) {
   return request.getRemoteAddr();
  }
  return request.getHeader("x-forwarded-for");
 }
	
}
