  
package com.example.spring.soap.api.services;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.log.LogAccessor;
import org.springframework.stereotype.Service;

import com.example.spring.soap.api.db.mongo.MongoDB;
import com.example.spring.soap.api.dto.Abilities;
import com.example.spring.soap.api.dto.HeldItem;
import com.example.spring.soap.api.dto.PokemonBody;

import org.apache.commons.logging.Log;
import org.example.estructura.AbilitiRequest;
import org.example.estructura.Ability;
import org.example.estructura.Item;
import org.example.estructura.PokemonRequest;
import org.example.estructura.PokemonResponse;
import org.example.estructura.Version;

@Service
public class PokemonService {
	@Value("${pokeom.api.service}")
	private String sUrlApi;

	public PokemonBody getBodyPokemon(String sPokemon) throws Exception {
		PokemonResponse pokemonResponse = new PokemonResponse();
		REST PokemonRest= new REST();
		PokemonBody pokeBody= new PokemonBody();

		if ((sPokemon.length() > 0)) {
			pokeBody=PokemonRest.obtenerBodyPokemon(sPokemon,sUrlApi);
		}
		
		return pokeBody;

	}
	
	@SuppressWarnings("null")
	public List<org.example.estructura.Abilities> validarAbiliti(String pokemonName, PokemonBody pokeBody) throws Exception {
		PokemonResponse acknowledgement = new PokemonResponse();
		REST PokemonRest= new REST();
		//PokemonBody pokeBody= new PokemonBody();
		List<Abilities> abilitiesBody = null;
		List<org.example.estructura.Abilities> ListaFinal = new ArrayList<org.example.estructura.Abilities>();

		if ((pokemonName.length() > 0)) {
			//pokeBody=PokemonRest.obtenerBodyPokemon(sPokemon,sUrlApi);
			if(pokeBody!=null) {
				abilitiesBody=pokeBody.getAbilities();
				for(int x=0;x<abilitiesBody.size();x++) {
					org.example.estructura.Abilities abilitiEstruct=new org.example.estructura.Abilities();
					Ability ability=new Ability();
					ability.setName(abilitiesBody.get(x).ability.name);
					ability.setUrl(abilitiesBody.get(x).ability.url);
					abilitiEstruct.setAbility(ability);
					
					ListaFinal.add(abilitiEstruct);
					
				}
			}
		}
		
		return ListaFinal;

	}
	
	public List<org.example.estructura.HeldItem> validarHeldItems(String pokemonName, PokemonBody pokeBody) throws Exception {
		PokemonResponse acknowledgement = new PokemonResponse();
		REST PokemonRest= new REST();
		//PokemonBody pokeBody= new PokemonBody();
		List<HeldItem> heldItemsBody = null;
		
		List<org.example.estructura.HeldItem> ListaFinal = new ArrayList<org.example.estructura.HeldItem>();
		List<org.example.estructura.VersionDetail> ListaVersionDetail = new ArrayList<org.example.estructura.VersionDetail>();

		if ((pokemonName.length() > 0)) {
			//pokeBody=PokemonRest.obtenerBodyPokemon(sPokemon,sUrlApi);
			if(pokeBody!=null) {
				heldItemsBody=pokeBody.getHeld_items();
				for(int x=0;x<heldItemsBody.size();x++) {
					org.example.estructura.HeldItem heldItemEstruct=new org.example.estructura.HeldItem();
					Item itemBase=new Item();
					itemBase.setName(heldItemsBody.get(x).item.name);
					itemBase.setUrl(heldItemsBody.get(x).item.url);
					heldItemEstruct.setItem(itemBase);
					for(int y=0;y<heldItemsBody.get(x).version_details.size();y++) {
						org.example.estructura.VersionDetail versionDetailEstruct=new org.example.estructura.VersionDetail();
						Version versionBase=new Version();
						versionBase.setName(heldItemsBody.get(x).version_details.get(y).version.name);
						versionBase.setUrl(heldItemsBody.get(x).version_details.get(y).version.url);
						versionDetailEstruct.setVersion(versionBase);
						versionDetailEstruct.setRarity(heldItemsBody.get(x).version_details.get(y).rarity);
						ListaVersionDetail.add(versionDetailEstruct);
					}
					heldItemEstruct.setVersionDetail(ListaVersionDetail);
					
					
					ListaFinal.add(heldItemEstruct);
					
				}
			}
		}
		
		return ListaFinal;

	}



	

}