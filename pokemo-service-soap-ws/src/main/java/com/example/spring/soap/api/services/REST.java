package com.example.spring.soap.api.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;


import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import com.example.spring.soap.api.dto.PokemonBody;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class REST {

  /**
   * Obtener tasas de interes por tipo ciudad, por regla de negocio se le enviar?
   * el tipociudad 1 que es ciudad interior
   * 
   * @return objeto json con relacion de tasas de interes por plazo
   * @throws Exception
   */
  public static PokemonBody obtenerBodyPokemon(String sPokemon,String sUrlApi) throws Exception {
    String resp = "";
    PokemonBody responseCache= new PokemonBody();
    try {
     
      String url =sUrlApi;
    
      URL urlServicio = new URL(url + "pokemon/"+sPokemon);
      Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();

      // String key = query.hashCode() + "";
      OkHttpClient client = new OkHttpClient().newBuilder().build();

      Request request = new Request.Builder().url(urlServicio).method("GET", null).build();
      ResponseBody responseBodyCache = client.newCall(request).execute().body();
       responseCache = gson.fromJson(responseBodyCache.string(), PokemonBody.class);
      resp = gson.toJson(responseCache);
      //LOG.debug("Respuesta servicio margen compra: " + resp);
    } catch (Exception e) {
      resp = "";
     // LOG.error("Error -> servicio margen compra: " + e);
    }

    //LOG.debug("Termina margen compra: ");

    return responseCache;
  }
}
