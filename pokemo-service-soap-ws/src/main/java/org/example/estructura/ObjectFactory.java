//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.09.29 a las 02:21:19 AM MDT 
//


package org.example.estructura;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.example.estructura package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.example.estructura
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AbilitiRequest }
     * 
     */
    public AbilitiRequest createAbilitiRequest() {
        return new AbilitiRequest();
    }

    /**
     * Create an instance of {@link BaseExperienceRequest }
     * 
     */
    public BaseExperienceRequest createBaseExperienceRequest() {
        return new BaseExperienceRequest();
    }

    /**
     * Create an instance of {@link IdResponse }
     * 
     */
    public IdResponse createIdResponse() {
        return new IdResponse();
    }

    /**
     * Create an instance of {@link NameRequest }
     * 
     */
    public NameRequest createNameRequest() {
        return new NameRequest();
    }

    /**
     * Create an instance of {@link PokemonRequest }
     * 
     */
    public PokemonRequest createPokemonRequest() {
        return new PokemonRequest();
    }

    /**
     * Create an instance of {@link LocationAreaEncountersResponse }
     * 
     */
    public LocationAreaEncountersResponse createLocationAreaEncountersResponse() {
        return new LocationAreaEncountersResponse();
    }

    /**
     * Create an instance of {@link PokemonResponse }
     * 
     */
    public PokemonResponse createPokemonResponse() {
        return new PokemonResponse();
    }

    /**
     * Create an instance of {@link NameResponse }
     * 
     */
    public NameResponse createNameResponse() {
        return new NameResponse();
    }

    /**
     * Create an instance of {@link HeldItemsResponse }
     * 
     */
    public HeldItemsResponse createHeldItemsResponse() {
        return new HeldItemsResponse();
    }

    /**
     * Create an instance of {@link HeldItem }
     * 
     */
    public HeldItem createHeldItem() {
        return new HeldItem();
    }

    /**
     * Create an instance of {@link LocationAreaEncountersRequest }
     * 
     */
    public LocationAreaEncountersRequest createLocationAreaEncountersRequest() {
        return new LocationAreaEncountersRequest();
    }

    /**
     * Create an instance of {@link IdRequest }
     * 
     */
    public IdRequest createIdRequest() {
        return new IdRequest();
    }

    /**
     * Create an instance of {@link HeldItemsRequest }
     * 
     */
    public HeldItemsRequest createHeldItemsRequest() {
        return new HeldItemsRequest();
    }

    /**
     * Create an instance of {@link BaseExperienceResponse }
     * 
     */
    public BaseExperienceResponse createBaseExperienceResponse() {
        return new BaseExperienceResponse();
    }

    /**
     * Create an instance of {@link AbilitiResponse }
     * 
     */
    public AbilitiResponse createAbilitiResponse() {
        return new AbilitiResponse();
    }

    /**
     * Create an instance of {@link Abilities }
     * 
     */
    public Abilities createAbilities() {
        return new Abilities();
    }

    /**
     * Create an instance of {@link Ability }
     * 
     */
    public Ability createAbility() {
        return new Ability();
    }

    /**
     * Create an instance of {@link Item }
     * 
     */
    public Item createItem() {
        return new Item();
    }

    /**
     * Create an instance of {@link Version }
     * 
     */
    public Version createVersion() {
        return new Version();
    }

    /**
     * Create an instance of {@link VersionDetail }
     * 
     */
    public VersionDetail createVersionDetail() {
        return new VersionDetail();
    }

}
